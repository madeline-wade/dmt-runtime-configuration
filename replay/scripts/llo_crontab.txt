SHELL=/bin/sh
MAILTO=patrick.brockill@ligo.org,john.zweizig@ligo.org
CRON_TZ=UTC
#
40 3,7,11,15,19,23 * * * $HOME/scripts/split-wave > /dev/null 2>&1
* * * * * /usr/bin/SPI.pl --gds-config=$HOME/dmtspi >/home/o3replay/logs/spi.log 2>&1
*/10 * * * * /usr/bin/USAGE.tcsh
02 07 * * * PATH=/usr/bin:/bin $HOME/scripts/purgedir /o3replay/frames/O3/detchar/L1/ L-L1_Online 350
45 13 * * * PATH=/usr/bin:/bin $HOME/scripts/next-flood
25 18 * * * PATH=/usr/bin:/bin $HOME/scripts/next-flood
