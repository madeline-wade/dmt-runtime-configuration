#
#    File:	LHO_Replay_ProcList
#
#    Purpose:	This ProcList file is used by the procsetup utility to 
#		create the node-specific configuration files for the
#		process manager.
#
#    Author:	J. Zweizig
#
#    Version:	1.0; Modified February 26, 2021
#    		Draft version for O3 E2E replay
#
#-
#
#    This file contains one configuration line for each process to be 
#    run. The configuration line may be continued over multiple text 
#    by ending a non-terminal line with a backslash (\). Comments start
#    with a hash-mark (#) and continue to the end of the line. The 
#    configuration line contains the following fields:
#
#    1) Process name
#    2) CPU usage
#    3) Command to be executed by the process
#    4) Node assignment constraints
#    5) Procmgr tag
#    6) Files used by the process
#
#    Each field should be enclosed in braces if it contains white-space 
#    characters. The command line may contain symbols or escape sequences
#    as allowed by the process manager. The "node assignment constraints" 
#    field is a perl expression that evaluates to true if the process may
#    be run on a given node. The symbols in this expression are substituted 
#    with values defined in the node list on a node-by-node basis.
#
#    Generate procmgt configure files from this file by running the 
#    following command:
#
#       procsetup --file:LHO_Replay
#
#==========================================================================
#
#=======================================  Infrastructure process definitions
#
#   Servers
#   Load unmeasured
#
NameServer 100	{dmt_nameserver} \
	  	{$comnode} \
	  	{-log -no-output} \
		{}
#
TriggerManager 100 \
	       {TrigMgr -ofile $DMTRIGDIR -ntrig 100 \
	       		-access TrigMgr_LHO_O3E2E.conf \
	       		-streams TrigStream_LHO_O3E2E.cfg} \
	       {$comnode} \
	       {-when:"waitProc dmt_nameserver 3" -log -no-output:data:trend} \
	       {TrigMgr_LHO_O3E2E.conf TrigStream_LHO_O3E2E.cfg}
#	       
AlarmManager 100 \
	       {AlarmMgr} \
	       {$comnode} \
	       {-when:"waitProc dmt_nameserver 3" -log -no-output} \
	       {}
#
#   Webview processes
#   Load unmeasured
#
webview_1 100	{webview -p 9991 -l $HOME/logs/webview_1.log} \
	  	{$comnode} \
	  	{-no-output} \
		{}
#
#   Set up raw data transfer to the ldas cluster.
link_raw  100  {framelink --delta 1 --stats 300 \
	       		  push $LIGOSMPART $LOWLATENCY_HOST:32134} \
	       {$comnode} \
	       {-no-output -log} \
	       {}
#
#=======================================  h(t) calibration process definitions
htCalib_H1 400 \
	       {$HOME/bin/htCalib} \
	       {$htpart} \
	       {-no-output:trend:data -log} \
	       {}
#
#---> Set up low-latency h(t) data transfer to the local cluster.
link_hoft 100  {framelink --delta 1 --stats 300 \
	       		  push $DQHOFTPART $LOWLATENCY_HOST:32135} \
	       {$htpart} \
	       {-no-output -log} \
	       {}
#
#---> Set up low-latency h(t) data transfer to the local cluster.
link_idq 100   {framelink --delta 1 --stats 300 \
                        pull $LOWLATENCY_HOST:32139 $IDQPART} \
             {$htpart} \
             {-no-output -log} \
             {}
#
#---> DMTGate generation utility
DMTGate_H1 100 {dq-module -inlist dq-stream-LHO_Online.txt \
               H-SelfGate_O3E2E.js} \
               {$htpart} \
               {-no-output} \
               {dq-stream-LHO_Online.txt H-SelfGate_O3E2E.js}
#
#---> Gate monitor
gating_monitor_H1 100 \
		{gatemonitor -inlist dq-stream-LHO_gate.txt \
	    	 H-GateMonitor_O3E2E.js} \
		{$htpart} \
             	{-no-output:data -log \
                 -env=TEMPLATE_TREND_SUBDIR=H-H1_GATEM_M-%6r} \
             	{dq-stream-LHO_gate.txt H-GateMonitor_O3E2E.js}
#
#---> DMTDQ generation utility
DMTDQ_H1 100    {dq-module -inlists dq-gating-LHO.txt H-DQmod_O3.json} \
                {$htpart} \
                {-no-output} \
                {dq-gating-LHO.txt dq-stream-LHO_Online.txt \
		 dq-stream-LHO_hoft.txt dq-stream-LHO_idq.txt H-DQmod_O3.json}
#
#---> Trend low-latency frames.
trender_H1 125	{trender  -n H-H1_DMT_C00_M -i H1 -l dq-stream-LHO_dmtdq.txt} \
                {$htpart} \
                {-no-output:data:html \
                 -env=DMTRENDOUT=$DMTRENDIR/$DMTPROCESS/H-H1_DMT_C00_M-%6r} \
                {dq-stream-LHO_dmtdq.txt}
#
#
#---> Science Segment generation
Science_Segs_H1 125 \
                {SegGener -osc hoft_dq_flags.osc -conf Science_Segs_H1.cnf \
                          -partition $DQHOFTPART +seg} \
                {$htpart} \
                {-no-output:trend:data} \
                {hoft_dq_flags.osc,Science_Segs_H1.cnf}
#
#=======================================  Monitoring process definitions
#
#
#---> Duotone
#     Load unmeasured
DuoTone_H1 200 \
	   {DuoTone --offset 72 -cSLSCX H1:CAL-PCALX_FPGA_DTONE_ADC_DQ \
 	   	                -cSLSCY H1:CAL-PCALY_FPGA_DTONE_ADC_DQ} \
           {$float} \
           {-log -no-output:data} \
           {}
#
#---> Test for data valid errors
dvTest	100 \
          {dvTest -conf dvTest_O3_LHO.cfg} \
 	  {$float} \
 	  {-no-output:data} \
 	  {dvTest_O3_LHO.cfg}
#
#---> End_Times
EndTimes_H1 200 \
	  {EndTimes -conf EndTimes_H1.cfg -osc LockLoss.osc} \
	  {$float} \
	  {-no-output:trend:data} \
	  {EndTimes_H1.cfg LockLoss.osc}
#
#
#---> kleineWelle Glitch detection
#     Note that this produces data to /dmt/triggers/
#kleineWelle_H1 2500 \
#		  {kleineWelleM $HOME/pars/H-O3b_KW_AUX.cfg \
#		  		-inlist $HOME/pars/default_in.txt} \
#	    	  {$float} \
#		  {-log -cd:$DMTRIGDIR/H-KW_TRIGGERS -no-output:trend:data} \
#		  {H-O3b_KW_AUX.cfg default_in.txt}
#
#---> Omega on calibrated h(t) data
#
Omega_HOFT_H1 300 \
              {dmt_wstream H1-HOFT_OMEGA.cfg H1-hoftStreamList.txt \
                           $DMTRIGDIR/H-HOFT_Omega/%5g 0} \
              {$htpart} \
              {-no-output:trend:data -log} \
              {H1-HOFT_OMEGA.cfg H1-hoftStreamList.txt H1-htpart_in.txt}
#
#
#   PCalMon
#   Parameters modified as requested by Greg 1/8/2015
PCalMon_H1 300 \
	      {PCalMon -c H1:CAL-CS_LINE_SUM_DQ -f 35.3 \
	      	       -c H1:CAL-DARM_ERR_WHITEN_OUT_DBL_DQ -f 33.7 \
		       -c H1:CAL-DARM_ERR_WHITEN_OUT_DBL_DQ -f 34.7 \
		       -c H1:CAL-DARM_ERR_WHITEN_OUT_DBL_DQ -f 35.3 \
		       -c H1:CAL-DARM_ERR_WHITEN_OUT_DBL_DQ -f 331.3 \
		       -c H1:CAL-DARM_ERR_WHITEN_OUT_DBL_DQ -f 1083.1 \
		       -c H1:CAL-DARM_ERR_WHITEN_OUT_DBL_DQ -f 3001.1 \
		       -c H1:CAL-PCALY_RX_PD_OUT_DQ -f 34.7 \
		       -c H1:CAL-PCALY_RX_PD_OUT_DQ -f 331.3 \
		       -c H1:CAL-PCALY_RX_PD_OUT_DQ -f 1083.1 \
		       -c H1:CAL-PCALX_RX_PD_OUT_DQ -f 3001.1 \
		       -stride 10 -OSCfile h1_locked.osc \
		       -OSCcond H1:IFO_LOCKED -debug 0} \
              {$comnode} \
              {-no-output:data -log} \
              {h1_locked.osc}
#
#   Seismic BLRMS
Seis_Blrms_H1 125 \
	      {blrms_monitor	-mons Seis_Blrms_H1 \
				-if $DMTPARS/blrmsconfig_LHO.conf \
				-trendname Seis_Blrms \
				-of $DMTOUTPUT/blrms%t.data -mtxt no} \
	      {$float} \
	      {-no-output:data} \
	      {blrmsconfig_LHO.conf half_decade_set_3.filter}
#
#   BLRMS from SEI STS seismometers
H1_STS_Blrms 125 \
	      {blrms_monitor	-mons H1_STS_Blrms \
				-if $DMTPARS/LHO_SEI_blrms.conf \
				-trendname H1_STS_Blrms \
				-of $DMTOUTPUT/blrms%t.data -mtxt no} \
	      {$float} \
	      {-no-output:data} \
	      {LHO_SEI_blrms.conf STS_Seis_Blrms.filter}
#
#   Segment generation
#   Load 68 Mips measured 5/16/2005 4.5% of 2x750
#
SegGener_H1 125	{SegGener -osc  SegGener_LHO.osc \
			  -conf SegGener_LHO.cnf +seg} \
		{$float} \
		{-no-output:trend:data} \
		{SegGener_LHO.osc SegGener_GRD_LHO.osc SegGener_LSC_LHO.osc \
		 SegGener_PI_LHO.osc SegGener_HWINJ_LHO.osc \
		 SegGener_LHO.cnf SegGener_GRD_LHO.cnf SegGener_LSC_LHO.cnf \
		 SegGener_PI_LHO.cnf SegGener_HWINJ_LHO.cnf}
#
#   NS Binary inspiral sensitivity
#
SenseMonitor_H1 125 \
		{SenseMonitor -config SenseMonitor_hoft_H1.conf \
                              -partition $DQHOFTPART} \
                {$htpart} \
                {-no-output:data} \
                {SenseMonitor_hoft_H1.conf ReferenceCalibration_hoft_H1.xml}
#
#   Binary BH inspiral sensitivity
#
SenseMonitor_BBH_30_30_H1 150 \
		{SenseMonitor -config $DMTPARS/SenseMonitor_BBH_30_30_H1.conf} \
		{$float} \
		{-no-output:data} \
		{SenseMonitor_BBH_30_30_H1.conf \
		ReferenceCalibrationDarmCal_H1.xml \
		data/SenseMonitor_BBH_30_30.txt}
#
#   NS Binary inspiral sensitivity
#
SenseMonitor_CAL_H1 125 \
		{SenseMonitor -config SenseMonitor_CAL_H1.conf \
                              -partition $LIGOSMPART} \
                {$htpart} \
                {-no-output:data} \
                {SenseMonitor_CAL_H1.conf ReferenceCalibrationDarmCal_H1.xml}
#
