{
    "config-version" : "LLO o3replay DQ+Gate configuration version 2.0",
    "stride" : 1.0,
    "file-alignment" : 1.0,
    "flag_step" : 0.0625,
    "write_path" : "/online/LLO_DQGATE",
    "compression" : "zero-suppress-or-gzip",
    "dq_flags" : [
	{
	    "name" : "L1:DMT-ETMX_ESD_DAC_OVERFLOW",
	    "channel" : "L1:DMT-DQ_VECTOR",
	    "bit" :  4,
	    "class" : "DQ_OvflCount",
	    "library" : "/usr/lib64/libdqplugins.so",
            "invert" : true,
	    "inputs" : [
                "0/L1:FEC-88_DAC_OVERFLOW_ACC_3_1",
                "0/L1:FEC-88_DAC_OVERFLOW_ACC_3_2",
                "0/L1:FEC-88_DAC_OVERFLOW_ACC_3_3",
                "0/L1:FEC-88_DAC_OVERFLOW_ACC_3_4"
	    ],
            "parameters" : {
                "print" : 0
            }
	},
	{
	    "name" : "L1:DMT-ETMY_ESD_DAC_OVERFLOW",
	    "channel" : "L1:DMT-DQ_VECTOR",
	    "bit" :  2,
	    "class" : "DQ_OvflCount",
	    "library" : "/usr/lib64/libdqplugins.so",
            "invert" : true,
	    "inputs" : [
                "0/L1:FEC-98_DAC_OVERFLOW_ACC_3_1",
                "0/L1:FEC-98_DAC_OVERFLOW_ACC_3_2",
                "0/L1:FEC-98_DAC_OVERFLOW_ACC_3_3",
                "0/L1:FEC-98_DAC_OVERFLOW_ACC_3_4"
	    ],
            "parameters" : {
                "print" : 0
            }
	},
        {
            "name" : "L1:DMT-OMC_DCPD_ADC_OVERFLOW",
            "channel" : "L1:DMT-DQ_VECTOR",
            "bit" :  1,
            "class" : "DQ_OvflCount",
            "library" : "/usr/lib64/libdqplugins.so",
            "invert" : true,
            "inputs" : [
                "0/L1:FEC-8_ADC_OVERFLOW_ACC_0_12",
                "0/L1:FEC-8_ADC_OVERFLOW_ACC_0_13"
            ],
            "parameters" : {
                "print" : 0
            }
        },
        {
            "name" : "L1:BLRMS_STRAIN_70_110",
            "channel" : "L1:DMT-DQ_VECTOR",
	    "test-channel" : "L1:DMT-BLRMS_STRAIN_70_110",
            "bit" :  5,
            "class" : "DQ_GenRms",
            "library" : "/usr/lib64/libdqplugins.so",
            "invert" : false,
            "inputs" : [
                "0/L1:CAL-DARM_ERR_DBL_DQ"
            ],
            "parameters" : {
		"rms-max" : 3e-12,
		"decay-time" : 0.0,
		"settle" : 2.0,
		"filter-design" : "ellip('BandPass',8,1,100.0,70,110)",
                "print" : 0
            }
        },
        {
            "name" : "L1:BLRMS_STRAIN_25_50",
            "channel" : "L1:DMT-DQ_VECTOR",
            "test-channel" : "L1:DMT-BLRMS_STRAIN_25_50",
            "bit" :  6,
            "class" : "DQ_GenRms",
            "library" : "/usr/lib64/libdqplugins.so",
            "invert" : false,
            "inputs" : [
                "0/L1:CAL-DARM_ERR_DBL_DQ"
            ],
            "parameters" : {
                "rms-max" : 1e-11,
                "decay-time" : 0.0,
                "settle" : 2.0,
                "filter-design" : "ellip('BandPass',8,1,100.0,25,50)",
                "print" : 0
            }
        }
    ],
    "filters" : [
        {"name"     : "strain-gate",
         "type"     : "GateGen",
         "rate"     : 16384,
         "transit"  : 0.25,
         "front"    : 0.375,
         "idle"     : 1.0,
         "active"   : 0.0,
         "mode"     : "&~",
         "bit-mask" : 118
        },
        {"name"     : "dqvec-gate",
         "type"     : "GateGen",
         "rate"     : 16,
         "transit"  : 0.25,
         "front"    : 0.375,
         "window"   : "rectangle",
         "idle"     : 0,
         "active"   : 9,
         "mode"     : "&~",
         "bit-mask" : 118
        },
        {"name" : "multiply",
         "type" : "math_2op",
         "rate" : 16384,
         "opcode" : "*"
        },
        {"name" : "xor",
         "type" : "logic_2op",
         "rate" : 16,
         "opcode" : "A^B"
        },
        {"name"   : "xor9",
         "type"   : "MathOp",
         "opcode" : "^",
         "value"  : 9
        }
    ],
    "processed-channels" : [
        {"name"    : "L1:GDS-STRAIN_GATE",
         "filter"  : "strain-gate",
         "x-input" : "L1:DMT-DQ_VECTOR"
        },
        {"name"    : "L-DQVECT_TEMP_1",
         "filter"  : "dqvec-gate",
         "x-input" : "L1:DMT-DQ_VECTOR",
         "mode"    : "nowrite"
        },
        {"name"    : "L-DQVECT_TEMP_2",
         "filter"  : "xor9",
         "x-input" : "L1:DMT-DQ_VECTOR",
         "mode"    : "nowrite"
        },
        {"name"    : "L1:DMT-DQ_VECTOR_GATED",
	 "filter"  : "xor",
	 "x-input" : "L-DQVECT_TEMP_1",
         "y-input" : "L-DQVECT_TEMP_2"
	}
    ],
    "copy_channels" : [
	"0/L1:GRD-ISC_LOCK_MODE",
	"0/L1:GRD-ISC_LOCK_ERROR",
	"0/L1:GRD-ISC_LOCK_STATE_N",
	"0/L1:GRD-IFO_READY"
    ]
}
