#
#   File: DataQual_LLO.conf
#
#   This file is the configuration for the data quality monitor. The
#   idea is to have a complete set of trends of RMS of narrow frequency
#   bands of IFO channels and of glitches. The tools needed are as follows:
#
#   1) We will trend the RMS of narrow bands on AS_Q, MC_F, POB_Q and REFL_I. 
#      For the moment I assume that we will use 1/12th decade points in the 
#     frequency range from 30Hz - 7.1kHz which correspond to 21% intervals.
#     The band limits will are nudged one way or the other to land on
#     integral Hz boundaries, giving:
#
#          26    31    38    46    56    68    83   100   121   147
#         178   215   261   316   383   464   562   681   825  1000
#        1212  1468  1778  2154  2610  3162  3831  4642  5623  6813  
#        8254 
#  
#     This gives 30 trends/channel which shouldn't be too terrible either in
#     terms of data storage or in terms of computing load. 
#  
#  2) Trend a few narrow bands around known line features as follows:
#  
#     Mode                 Channel      Frequency
#     Violin 1st harmonic  AS_Q         343- 348Hz
#     Violin 2nd harmonic  AS_Q         680- 690Hz
#     Power line           AS_Q          58-  62Hz
#     Power 2nd harmonic   AS_Q         118- 122Hz
#     MC Violin 1st        REFL_I       638- 652Hz 
#     MC Violin 2nd        REFL_I      1285-1305Hz 
#     BS Roll Mode         POB_Q         16-  20Hz
#     
#  3) Glitch rates. Each channel will produce 6 trends with 4, 6sigma
#     thresholds and hi-pass filtering of 0, 10Hz, 100Hz. The following
#     channels will have glitch monitors
#  
#     AS_Q REFL_I POB_I POB_Q DARM_CTRL MC_L MICH_CTRL PRC_CTRL
#     HAM4_MIC HAM8_MIC (LHO only).
#
#  Notes: 
#  
#   a) I talked with Gaby after the meeting and incorporated her suggestions
#      to trend bands in POB_Q monitor BS roll band and glitches on all Ctrl
#      signals and HAM1_MIC. (Rana says HAM4_MIC is better 5/20/03)
#   b) All bands of IFO channels will be zeroed when IFO is not in lock.
#      Band values will be power (i.e. the variance or sum of the squares)
#      rather than the sigma so that the trend averaging works correctly.
#      Channels will be windowed with a Hanning window.
#   c) I expect that I can combine this all into a single PSLmon process.
#      FWIW, I intend to do calculations in a stride of 4 seconds.
#
#   Mods for S3 (jgz, 10/24/03)
#
#   A) Change osc condition to common mode (L1:Both_arms_locked_strict_cm)
#   B) Add AS_DC bands of 1-10, 10-30, 30-100 and 100-300Hz
#
#   Mods for S4 (jgz,  2/21/05)
#
#   A) Notch out calibration lines in at 396.7, 54.7, 1151.5
#
#   Mods for S6
#   A) Replace AS_Q with DARM_ERR
#
Parameter MonName DataQual
Parameter ChanPfx DTQL
Parameter TrigEnable 0
Parameter Stride 4.0
Parameter OSCFile "LockLoss.conf"
#
Filter HP16kHz100 Design -settle 0.2 \
       16384 "ellip('HighPass',6,1,120,100)*notch(120,400)*notch(180,400)"
#
Filter HP100DarmErr Design -settle 0.2 16384 \
       "ellip('HighPass',6,1,120,100)*notch(180,100)*notch(396.7,100)*notch(1151.5,200)*ellip('BandStop',4,3,90,3710,3745)*gain(1.414)"
#
Filter HP2kHz100  Design -settle 0.2 \
        2048 "ellip('HighPass',6,1,120,100)*notch(120,400)*notch(180,400)"
#
Filter HP16kHz500 Design -settle 0.2 \
       16384 "ellip('HighPass',6,1,120,500)"
Filter HP500DarmErr Design -settle 0.2 16384 \
       "ellip('HighPass',6,1,120,500)*notch(1151.5,200)*ellip('BandStop',4,3,90,3710,3745)*gain(1.414)"
#
Filter HP2kHz30   Design -settle 0.2 \
        2048 "ellip('HighPass',6,1,120,30)"

Filter HP2kHz100   Design -settle 0.2 \
        2048 "ellip('HighPass',6,1,120,100)"
#
Filter Hann Hanning
#
#    List all channels that I will need. Use Hanning window for FFTs.
#
Channel L1-DARM_ERR      L1:LSC-DARM_ERR   -window Hann
Channel L1-AS_DC     L1:LSC-AS_DC      -window Hann
Channel L1-MC_F      L1:IOO-MC_F       -window Hann
Channel L1-REFL_I    L1:LSC-REFL_I     -window Hann
Channel L1-POB_I     L1:LSC-POB_I
Channel L1-POB_Q     L1:LSC-POB_Q      -window Hann
Channel L1-DARM_CTRL L1:LSC-DARM_CTRL
Channel L1-MICH_CTRL L1:LSC-MICH_CTRL
Channel L1-PRC_CTRL  L1:LSC-PRC_CTRL
Channel L1-MC_L      L1:LSC-MC_L
#
#    Define bands
#
Band L1DERR1    L1-DARM_ERR   -fmin   26 -fmax   31 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR2    L1-DARM_ERR   -fmin   31 -fmax   38 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR3    L1-DARM_ERR   -fmin   38 -fmax   46 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR4    L1-DARM_ERR   -fmin   46 -fmax   56 \
		-while L1:Both_arms_locked_strict_cm -exclude 54.25-55.25
Band L1DERR5    L1-DARM_ERR   -fmin   56 -fmax   68 \
		-while L1:Both_arms_locked_strict_cm -exclude 59.5-60.75
Band L1DERR6    L1-DARM_ERR   -fmin   68 -fmax   83 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR7    L1-DARM_ERR   -fmin   83 -fmax  100 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR8    L1-DARM_ERR   -fmin  100 -fmax  121 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR9    L1-DARM_ERR   -fmin  121 -fmax  147 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR10   L1-DARM_ERR   -fmin  147 -fmax  178 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR11   L1-DARM_ERR   -fmin  178 -fmax  215 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR12   L1-DARM_ERR   -fmin  215 -fmax  261 \
		-while L1:Both_arms_locked_strict_cm -exclude 239.5-240.75
Band L1DERR13   L1-DARM_ERR   -fmin  261 -fmax  316 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR14   L1-DARM_ERR   -fmin  316 -fmax  383 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR15   L1-DARM_ERR   -fmin  383 -fmax  464 \
		-while L1:Both_arms_locked_strict_cm -exclude 396.5-397.25
Band L1DERR16   L1-DARM_ERR   -fmin  464 -fmax  562 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR17   L1-DARM_ERR   -fmin  562 -fmax  681 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR18   L1-DARM_ERR   -fmin  681 -fmax  825 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR19   L1-DARM_ERR   -fmin  825 -fmax 1000 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR20   L1-DARM_ERR   -fmin 1000 -fmax 1212 \
		-while L1:Both_arms_locked_strict_cm -exclude 1151.25-1152
Band L1DERR21   L1-DARM_ERR   -fmin 1212 -fmax 1468 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR22   L1-DARM_ERR   -fmin 1468 -fmax 1778 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR23   L1-DARM_ERR   -fmin 1778 -fmax 2154 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR24   L1-DARM_ERR   -fmin 2154 -fmax 2610 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR25   L1-DARM_ERR   -fmin 2610 -fmax 3162 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR26   L1-DARM_ERR   -fmin 3162 -fmax 3831 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR27   L1-DARM_ERR   -fmin 3831 -fmax 4642 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR28   L1-DARM_ERR   -fmin 4642 -fmax 5623 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR29   L1-DARM_ERR   -fmin 5623 -fmax 6813 \
		-while L1:Both_arms_locked_strict_cm
Band L1DERR30   L1-DARM_ERR   -fmin 6813 -fmax 8190 \
		-while L1:Both_arms_locked_strict_cm
#
Band L1POBQ1   L1-POB_Q  -fmin   26 -fmax   31 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ2   L1-POB_Q  -fmin   31 -fmax   38 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ3   L1-POB_Q  -fmin   38 -fmax   46 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ4   L1-POB_Q  -fmin   46 -fmax   56 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ5   L1-POB_Q  -fmin   56 -fmax   68 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ6   L1-POB_Q  -fmin   68 -fmax   83 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ7   L1-POB_Q  -fmin   83 -fmax  100 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ8   L1-POB_Q  -fmin  100 -fmax  121 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ9   L1-POB_Q  -fmin  121 -fmax  147 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ10  L1-POB_Q  -fmin  147 -fmax  178 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ11  L1-POB_Q  -fmin  178 -fmax  215 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ12  L1-POB_Q  -fmin  215 -fmax  261 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ13  L1-POB_Q  -fmin  261 -fmax  316 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ14  L1-POB_Q  -fmin  316 -fmax  383 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ15  L1-POB_Q  -fmin  383 -fmax  464 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ16  L1-POB_Q  -fmin  464 -fmax  562 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ17  L1-POB_Q  -fmin  562 -fmax  681 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ18  L1-POB_Q  -fmin  681 -fmax  825 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ19  L1-POB_Q  -fmin  825 -fmax 1000 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ20  L1-POB_Q  -fmin 1000 -fmax 1212 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ21  L1-POB_Q  -fmin 1212 -fmax 1468 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ22  L1-POB_Q  -fmin 1468 -fmax 1778 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ23  L1-POB_Q  -fmin 1778 -fmax 2154 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ24  L1-POB_Q  -fmin 2154 -fmax 2610 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ25  L1-POB_Q  -fmin 2610 -fmax 3162 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ26  L1-POB_Q  -fmin 3162 -fmax 3831 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ27  L1-POB_Q  -fmin 3831 -fmax 4642 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ28  L1-POB_Q  -fmin 4642 -fmax 5623 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ29  L1-POB_Q  -fmin 5623 -fmax 6813 \
		-while L1:Both_arms_locked_strict_cm
Band L1POBQ30  L1-POB_Q  -fmin 6813 -fmax 8190 \
		-while L1:Both_arms_locked_strict_cm
#
Band L1REFLI1  L1-REFL_I -fmin   26 -fmax   31 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI2  L1-REFL_I -fmin   31 -fmax   38 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI3  L1-REFL_I -fmin   38 -fmax   46 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI4  L1-REFL_I -fmin   46 -fmax   56 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI5  L1-REFL_I -fmin   56 -fmax   68 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI6  L1-REFL_I -fmin   68 -fmax   83 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI7  L1-REFL_I -fmin   83 -fmax  100 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI8  L1-REFL_I -fmin  100 -fmax  121 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI9  L1-REFL_I -fmin  121 -fmax  147 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI10 L1-REFL_I -fmin  147 -fmax  178 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI11 L1-REFL_I -fmin  178 -fmax  215 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI12 L1-REFL_I -fmin  215 -fmax  261 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI13 L1-REFL_I -fmin  261 -fmax  316 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI14 L1-REFL_I -fmin  316 -fmax  383 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI15 L1-REFL_I -fmin  383 -fmax  464 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI16 L1-REFL_I -fmin  464 -fmax  562 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI17 L1-REFL_I -fmin  562 -fmax  681 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI18 L1-REFL_I -fmin  681 -fmax  825 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI19 L1-REFL_I -fmin  825 -fmax 1000 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI20 L1-REFL_I -fmin 1000 -fmax 1212 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI21 L1-REFL_I -fmin 1212 -fmax 1468 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI22 L1-REFL_I -fmin 1468 -fmax 1778 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI23 L1-REFL_I -fmin 1778 -fmax 2154 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI24 L1-REFL_I -fmin 2154 -fmax 2610 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI25 L1-REFL_I -fmin 2610 -fmax 3162 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI26 L1-REFL_I -fmin 3162 -fmax 3831 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI27 L1-REFL_I -fmin 3831 -fmax 4642 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI28 L1-REFL_I -fmin 4642 -fmax 5623 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI29 L1-REFL_I -fmin 5623 -fmax 6813 \
		-while L1:Both_arms_locked_strict_cm
Band L1REFLI30 L1-REFL_I -fmin 6813 -fmax 8190 \
		-while L1:Both_arms_locked_strict_cm
#
Band L1MCF1    L1-MC_F   -fmin   26 -fmax   31 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF2    L1-MC_F   -fmin   31 -fmax   38 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF3    L1-MC_F   -fmin   38 -fmax   46 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF4    L1-MC_F   -fmin   46 -fmax   56 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF5    L1-MC_F   -fmin   56 -fmax   68 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF6    L1-MC_F   -fmin   68 -fmax   83 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF7    L1-MC_F   -fmin   83 -fmax  100 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF8    L1-MC_F   -fmin  100 -fmax  121 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF9    L1-MC_F   -fmin  121 -fmax  147 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF10   L1-MC_F   -fmin  147 -fmax  178 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF11   L1-MC_F   -fmin  178 -fmax  215 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF12   L1-MC_F   -fmin  215 -fmax  261 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF13   L1-MC_F   -fmin  261 -fmax  316 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF14   L1-MC_F   -fmin  316 -fmax  383 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF15   L1-MC_F   -fmin  383 -fmax  464 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF16   L1-MC_F   -fmin  464 -fmax  562 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF17   L1-MC_F   -fmin  562 -fmax  681 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF18   L1-MC_F   -fmin  681 -fmax  825 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF19   L1-MC_F   -fmin  825 -fmax 1000 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF20   L1-MC_F   -fmin 1000 -fmax 1212 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF21   L1-MC_F   -fmin 1212 -fmax 1468 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF22   L1-MC_F   -fmin 1468 -fmax 1778 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF23   L1-MC_F   -fmin 1778 -fmax 2154 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF24   L1-MC_F   -fmin 2154 -fmax 2610 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF25   L1-MC_F   -fmin 2610 -fmax 3162 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF26   L1-MC_F   -fmin 3162 -fmax 3831 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF27   L1-MC_F   -fmin 3831 -fmax 4642 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF28   L1-MC_F   -fmin 4642 -fmax 5623 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF29   L1-MC_F   -fmin 5623 -fmax 6813 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCF30   L1-MC_F   -fmin 6813 -fmax 8190 \
		-while L1:Both_arms_locked_strict_cm
#
#   Bands around narrow features...
#
#     Mode                 Channel      Frequency
#     Violin 1st harmonic  AS_Q         340- 345Hz
#     Violin 2nd harmonic  AS_Q         680- 690Hz
#     Power line           AS_Q          58-  62Hz
#     Power 2nd harmonic   AS_Q         118- 122Hz
#     MC Violin 1st        REFL_I       638- 652Hz 
#     MC Violin 2nd        REFL_I      1285-1305Hz 
#     BS Roll Mode         POB_Q         16-  20Hz
#
Band L1Power1  L1-DARM_ERR   -fmin   58 -fmax   62 \
		-while L1:Both_arms_locked_strict_cm
Band L1Power2  L1-DARM_ERR   -fmin  118 -fmax  122 \
		-while L1:Both_arms_locked_strict_cm
Band L1Viol1   L1-DARM_ERR   -fmin  342 -fmax  349 \
		-while L1:Both_arms_locked_strict_cm
Band L1Viol2   L1-DARM_ERR   -fmin  684 -fmax  698 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCViol1 L1-REFL_I -fmin  638 -fmax  652 \
		-while L1:Both_arms_locked_strict_cm
Band L1MCViol2 L1-REFL_I -fmin 1285 -fmax 1305 \
		-while L1:Both_arms_locked_strict_cm
Band L1BSRoll  L1-POB_Q  -fmin   16 -fmax   20 \
		-while L1:Both_arms_locked_strict_cm
#
# WIde Bands
#
Band L1DarmErr_10_100 L1-DARM_ERR -fmin 10 -fmax   100 \
		-while L1:Both_arms_locked_strict_cm \
		-exclude 54.5-55.5 -exclude 59.5-60.75
Band L1DarmErr_100_200 L1-DARM_ERR -fmin 100 -fmax 200 \
		-while L1:Both_arms_locked_strict_cm \
		-exclude 119.5-120.75 -exclude 179.5-180.75
#
#--- Exclude:
#    239.5 - 240.5   Power 6th harmonic
#    340.0 - 350.75  Violin modes
#    396.0 - 397.25  Calibration (396.7)
Band L1DarmErr_200_400 L1-DARM_ERR -fmin 200 -fmax 400 \
		-while L1:Both_arms_locked_strict_cm \
		-exclude 239.5-240.75 -exclude 340-351 -exclude 396-397.5
Band L1DarmErr_100_400 L1-DARM_ERR -fmin 100 -fmax 400 \
		-while L1:Both_arms_locked_strict_cm
#
#--- Exclude:
#    680.0 - 
Band L1DarmErr_400_1000 L1-DARM_ERR -fmin 400 -fmax 1000 \
		-while L1:Both_arms_locked_strict_cm -exclude 680.0-700.0
#
#
Band L1DarmErr_1000_7000 L1-DARM_ERR -fmin 1000 -fmax 7000 \
		-while L1:Both_arms_locked_strict_cm -exclude 1151-1152.25 \
		-exclude 3806.0-3808.25 -exclude 3725.50-3730.0
#
#   Wide Ad_DC bands
#
Band L1ASD1   L1-AS_DC   -fmin   1 -fmax  10 \
		-while L1:Both_arms_locked_strict_cm
Band L1ASD2   L1-AS_DC   -fmin  10 -fmax  30 \
		-while L1:Both_arms_locked_strict_cm
Band L1ASD3   L1-AS_DC   -fmin  30 -fmax 100 \
		-while L1:Both_arms_locked_strict_cm
Band L1ASD4   L1-AS_DC   -fmin 100 -fmax 300 \
		-while L1:Both_arms_locked_strict_cm
#
#    Finally, it's glitch time
#
Glitch L1DERR_4s_500f L1-DARM_ERR    -sigma 4 -filter HP500DarmErr \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1DERR_4s_100f L1-DARM_ERR    -sigma 4 -filter HP100DarmErr \
		  -while L1:Both_arms_locked_strict_cm \
		  -alarm 2.0 -atime 20.0
Glitch L1DERR_6s_500f L1-DARM_ERR    -sigma 6 -filter HP500DarmErr \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1DERR_6s_100f L1-DARM_ERR    -sigma 6 -filter HP100DarmErr \
		  -while L1:Both_arms_locked_strict_cm
#
Glitch L1RFLI_4s_500f L1-REFL_I -sigma 4 -filter HP16kHz500 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1RFLI_4s_100f L1-REFL_I -sigma 4 -filter HP16kHz100 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1RFLI_6s_500f L1-REFL_I -sigma 6 -filter HP16kHz500 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1RFLI_6s_100f L1-REFL_I -sigma 6 -filter HP16kHz100 \
		  -while L1:Both_arms_locked_strict_cm
#
Glitch L1POBQ_4s_500f L1-POB_Q  -sigma 4 -filter HP16kHz500 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1POBQ_4s_100f L1-POB_Q  -sigma 4 -filter HP16kHz100 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1POBQ_6s_500f L1-POB_Q  -sigma 6 -filter HP16kHz500 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1POBQ_6s_100f L1-POB_Q  -sigma 6 -filter HP16kHz100 \
		  -while L1:Both_arms_locked_strict_cm
#
Glitch L1POBI_4s_500f L1-POB_I  -sigma 4 -filter HP16kHz500 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1POBI_4s_100f L1-POB_I  -sigma 4 -filter HP16kHz100 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1POBI_6s_500f L1-POB_I  -sigma 6 -filter HP16kHz500 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1POBI_6s_100f L1-POB_I  -sigma 6 -filter HP16kHz100 \
		  -while L1:Both_arms_locked_strict_cm
#
Glitch L1DARMC_4s_500f L1-DARM_CTRL -sigma 4 -filter HP16kHz500 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1DARMC_4s_100f L1-DARM_CTRL -sigma 4 -filter HP16kHz100 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1DARMC_6s_500f L1-DARM_CTRL -sigma 6 -filter HP16kHz500 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1DARMC_6s_100f L1-DARM_CTRL -sigma 6 -filter HP16kHz100 \
		  -while L1:Both_arms_locked_strict_cm
#
Glitch L1MICHC_4s_500f L1-MICH_CTRL -sigma 4 -filter HP16kHz500 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1MICHC_4s_100f L1-MICH_CTRL -sigma 4 -filter HP16kHz100 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1MICHC_6s_500f L1-MICH_CTRL -sigma 6 -filter HP16kHz500 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1MICHC_6s_100f L1-MICH_CTRL -sigma 6 -filter HP16kHz100 \
		  -while L1:Both_arms_locked_strict_cm
#
Glitch L1PRCC_4s_500f L1-PRC_CTRL   -sigma 4 -filter HP16kHz500 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1PRCC_4s_100f L1-PRC_CTRL   -sigma 4 -filter HP16kHz100 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1PRCC_6s_500f L1-PRC_CTRL   -sigma 6 -filter HP16kHz500 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1PRCC_6s_100f L1-PRC_CTRL   -sigma 6 -filter HP16kHz100 \
		  -while L1:Both_arms_locked_strict_cm
#
Glitch L1MCL_4s_500f L1-MC_L   -sigma 4 -filter HP16kHz500 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1MCL_4s_100f L1-MC_L   -sigma 4 -filter HP16kHz100 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1MCL_6s_500f L1-MC_L   -sigma 6 -filter HP16kHz500 \
		  -while L1:Both_arms_locked_strict_cm
Glitch L1MCL_6s_100f L1-MC_L   -sigma 6 -filter HP16kHz100 \
		  -while L1:Both_arms_locked_strict_cm
#
