#
#   Specialized Glitch Monitor
#   __________________________
#
#   This monitor looks at known glitch sources and generates online segments
#   when those glitches are seen.
#
#   Date/Author  Changes
#   Feb 4, 2010/ CCosta, JZweizig  Remove most channels. Add TCS.
#   May 28, 2009 Remove H2 monitoring
#   May  4, 2009 S6 Version: 
#                  * Replace AS_Q with DARM_ERR.
#                  * Remove trigger enable
#                  * Add segment generation.
#
Parameter MonName ZGlitch
Parameter TrigEnable 1
Parameter Stride 4.0
Parameter OSCFile "/export/home/ops/pars/LockLoss.conf"
#
# ---> Filters for hipass, Power lines, Pulsars, etc.
Filter HP2048  Design -settle 0.25 2048 "ellip('HighPass',4,0.05,80,50)"
Filter notch_60Hz Design -settle 2.5   2048 "notch(60,100)*notch(300,100)"
#
# ---> S4 Pulsar frequencies are listed in:
#      http://blue/scirun/S4/HardwareInjection/Details/pulsar/s4try2_H1
Filter notch_pulsar Design -settle 2.0 16384 "notch(265.58,100)*notch(849.08,100)*notch(575.16,100)*notch(108.86,100)*notch(763.85,100)*notch(1403.16,100)*notch(52.81,100)*notch(148.72,100)*notch(1220.98,100)*notch(194.31,100)"
#
# ---> Calibration line notches for H1
Filter notch_calib1 Design -settle 2.0 16384 "notch(46.7,100)*notch(393.1,100)*notch(1144.3,100)"
#
# ---> Channels to be observed
Channel H0:PEM-LVEA1_V1
Channel H0:PEM-LVEA1_V2
Channel H0:PEM-LVEA1_V3
Channel H0:PEM-EX_V1
Channel H0:PEM-EY_V1
Channel H0:PEM-LVEA2_V1
Channel H0:PEM-LVEA2_V2
Channel H0:PEM-LVEA2_V3
Channel H0:PEM-MX_V1
Channel H0:PEM-MY_V1
#
Channel H1:LSC-DARM_CTRL_EXC_DAQ
Channel H1:LSC-ETMX_EXC_DAQ
#
# TCS Channels
Channel H1:TCS-ITMX_PD_ISS_OUT_AC
Channel H1:TCS-ITMY_PD_ISS_OUT_AC
#
#  Power lines
#
Glitch H0:PEM-LVEA1_V1 H0:PEM-LVEA1_V1  -sigma 5 -snip 0.02 \
 					-filter notch_60Hz 
Glitch H0:PEM-LVEA1_V2 H0:PEM-LVEA1_V2  -sigma 5 -snip 0.02 \
 					-filter notch_60Hz 
Glitch H0:PEM-LVEA1_V3 H0:PEM-LVEA1_V3  -sigma 5 -snip 0.02 \
 					-filter notch_60Hz 
Glitch H0:PEM-EX_V1    H0:PEM-EX_V1     -sigma 5 -snip 0.02 \
 					-filter notch_60Hz 
Glitch H0:PEM-EY_V1    H0:PEM-EY_V1     -sigma 5 -snip 0.02 \
 					-filter notch_60Hz 
Glitch H0:PEM-LVEA2_V1 H0:PEM-LVEA2_V1  -sigma 5 -snip 0.02 \
 					-filter notch_60Hz 
Glitch H0:PEM-LVEA2_V2 H0:PEM-LVEA2_V2  -sigma 5 -snip 0.02 \
 					-filter notch_60Hz 
Glitch H0:PEM-LVEA2_V3 H0:PEM-LVEA2_V3  -sigma 5 -snip 0.02 \
 					-filter notch_60Hz 
Glitch H0:PEM-MX_V1    H0:PEM-MX_V1     -sigma 5 -snip 0.02 \
 					-filter notch_60Hz 
Glitch H0:PEM-MY_V1    H0:PEM-MY_V1     -sigma 5 -snip 0.02 \
 					-filter notch_60Hz 
#
#  H1 Input Optics
Glitch  H1:LSC-ETMX_EXC_DAQ H1:LSC-ETMX_EXC_DAQ -sigma 5.0 -snip 0.01 \
 	-filter notch_pulsar
Glitch  H1:LSC-ETMX_EXC H1:LSC-ETMX_EXC -sigma 5.0 -snip 0.01 \
 	-filter notch_pulsar
#
# -- Follow calibration lines.
Glitch  H1:LSC-DARM_CTRL_EXC_DAQ H1:LSC-DARM_CTRL_EXC_DAQ -sigma 5 \
 	-size 0.0001 -snip 0.1 -filter notch_calib1 \
        -while H1:Both_arms_locked_strict_cm \
 	-segment H1:DMT-CALIB_LINE_GLITCH:1 \
 	-comment "H1 Calibration line glitches"
#
#   TCS Glitches
#
Glitch H1:TCS-ITMX_PD_ISS_OUT_ACL H1:TCS-ITMX_PD_ISS_OUT_AC \
            -sigma 15 -snip 0.01 -while H1:Both_arms_locked_strict_cm \
            -filter HP2048 -segment H1:DMT-TCS_ITMX_OUT_LOW    \
            -comment "H1 TCS ITMX glitches 15 sigma"
Glitch H1:TCS-ITMX_PD_ISS_OUT_ACM H1:TCS-ITMX_PD_ISS_OUT_AC \
            -sigma 30 -snip 0.01 -while H1:Both_arms_locked_strict_cm \
            -filter HP2048 -segment H1:DMT-TCS_ITMX_OUT_MED    \
            -comment "H1 TCS ITMX glitches 30 sigma"
Glitch H1:TCS-ITMX_PD_ISS_OUT_ACH H1:TCS-ITMX_PD_ISS_OUT_AC \
            -sigma 45 -snip 0.01 -while H1:Both_arms_locked_strict_cm \
            -filter HP2048 -segment H1:DMT-TCS_ITMX_OUT_HIGH    \
            -comment "H1 TCS ITMX glitches 45 sigma"
#
Glitch H1:TCS-ITMY_PD_ISS_OUT_ACL H1:TCS-ITMY_PD_ISS_OUT_AC \
            -sigma 15 -snip 0.01 -while H1:Both_arms_locked_strict_cm \
            -filter HP2048 -segment H1:DMT-TCS_ITMY_OUT_LOW    \
            -comment "H1 TCS ITMY glitches - 15 sigma"
Glitch H1:TCS-ITMY_PD_ISS_OUT_ACM H1:TCS-ITMY_PD_ISS_OUT_AC \
            -sigma 30 -snip 0.01 -while H1:Both_arms_locked_strict_cm \
            -filter HP2048 -segment H1:DMT-TCS_ITMY_OUT_MED    \
            -comment "H1 TCS ITMY glitches 30 sigma"
Glitch H1:TCS-ITMY_PD_ISS_OUT_ACH H1:TCS-ITMY_PD_ISS_OUT_AC \
            -sigma 45 -snip 0.01 -while H1:Both_arms_locked_strict_cm \
            -filter HP2048 -segment H1:DMT-TCS_ITMY_OUT_HIGH    \
            -comment "H1 TCS ITMY glitches 45 sigma"
#
