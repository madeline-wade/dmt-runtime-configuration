#
# OSC configuration for monitoring violin mode in Mode Cleaner
# $Id$
#


# Information from Fred Raab, 
# http://blue.ligo-wa.caltech.edu/ilog/pub/ilog.cgi?group=detector&date_to_view=12/15/2002&anchor_to_scroll_to=2002:12:15:01:33:39-fjr
#
# and from Rana,
# http://www.ligo-la.caltech.edu/ilog/pub/ilog.cgi?group=detector&date_to_view=09/05/2002&anchor_to_scroll_to=2002:09:05:02:43:50-rana
#
# Fred suggests changing the monitored bands and thresholds to:
#
# H1:
#    343 - 348 Hz ; 1e-16 mRMS
#    686 - 696 Hz ; 1e-16 mRMS
#
# H2:
#    TBA
#
# L1:
#    (ditto H1)
#
# ALSO NOTE: Fred watches AS_Q, while the current ServoMon config watches
#            DARM_CTRL
#
# Use calibration data from S1 available at Calib. Group's web page:
#    http://blue.ligo-wa.caltech.edu/engrun/Calib_Home/
# to convert meters/sqrt(Hz) to counts/sqrt(Hz)
#
# In particular, for H1,
#   http://blue.ligo-wa.caltech.edu/scirun/S1/Results/CalibrationStability/temporal/H1ASQcalibration.txt
# For the range 343-348Hz, the magnitude of calibration (we ignore
# the phase), is a straight line, so we take the average: 
#   2.2676e-18 strain/count
# For the range 686-696Hz, the magnitude is also close to a straight
# line, so the average is:
#   1.0691e-17 strain/count


include "LockLoss.conf"

#######################
#                    #
#  Begin H1 Section #
#                  #
###################

H1:MC_F_peaking        abspowerabove "H1:IOO-MC_F"  freqlo=3000 freqhi=7000 threshold=950.
H1:MC_problem          boolean       "H1:Both_arms_locked & H1:MC_F_peaking"

# Violin mode fundamental
H1:DARM_CTRL_violin_0  abspowerabove "H1:LSC-DARM_CTRL" freqlo=343 freqhi=348 threshold=0.3
H1:DARM_problem_0      boolean       "H1:Both_arms_locked & H1:DARM_CTRL_violin_0"

# Close to the violin mode fundamental
H1:DARM_CTRL_violin_0_plus  abspowerabove "H1:LSC-DARM_CTRL" freqlo=345 freqhi=355 threshold=0.45
H1:DARM_problem_0_plus      boolean       "H1:Both_arms_locked & H1:DARM_CTRL_violin_0_plus"

# Violin mode 2nd harmonic
H1:DARM_CTRL_violin_1  abspowerabove "H1:LSC-DARM_CTRL" freqlo=686 freqhi=696 threshold=2.2
H1:DARM_problem_1      boolean       "H1:Both_arms_locked & H1:DARM_CTRL_violin_1"

# 400-450 Hz
H1:DARM_CTRL_400_450   abspowerabove "H1:LSC-DARM_CTRL" freqlo=400 freqhi=450 threshold=1000.
H1:DARM_problem_2      boolean       "H1:Both_arms_locked & H1:DARM_CTRL_400_450"

###################
#                  #
#  End H1 Section   #
#                    #
#######################


#######################
#                    #
#  Begin H2 Section #
#                  #
###################

#H2:MC_F_peaking        abspowerabove "H2:IOO-MC_F"  freqlo=3000 freqhi=7000 threshold=470.
#H2:MC_problem          boolean       "H2:Both_arms_locked & H2:MC_F_peaking"

### The following two are for DEBUGGING. Use the above two for production.
H2:MC_F_peaking        abspowerabove "H2:IOO-MC_F"  freqlo=3000 freqhi=7000 threshold=50.
H2:MC_problem          boolean       "H2:MC_F_peaking"

# Violin mode fundamental
H2:DARM_CTRL_violin_0  abspowerabove "H2:LSC-DARM_CTRL" freqlo=340 freqhi=345 threshold=0.25
H2:DARM_problem_0      boolean       "H2:Both_arms_locked & H2:DARM_CTRL_violin_0"

# Close to the violin mode fundamental
H2:DARM_CTRL_violin_0_plus  abspowerabove "H2:LSC-DARM_CTRL" freqlo=345 freqhi=355 threshold=0.5
H2:DARM_problem_0_plus      boolean       "H2:Both_arms_locked & H2:DARM_CTRL_violin_0_plus"

# Violin mode 2nd harmonic
H2:DARM_CTRL_violin_1  abspowerabove "H2:LSC-DARM_CTRL" freqlo=680 freqhi=690 threshold=0.07
H2:DARM_problem_1      boolean       "H2:Both_arms_locked & H2:DARM_CTRL_violin_1"

# 400-450 Hz
H2:DARM_CTRL_400_450   abspowerabove "H2:LSC-DARM_CTRL" freqlo=400 freqhi=450 threshold=1000.
H2:DARM_problem_2      boolean       "H2:Both_arms_locked & H2:DARM_CTRL_400_450"

# DEBUGGING
#H2:DEBUG_1             meanabove    "H2:LSC-LA_PTRR_NORM"     threshold=0.0
#H2:DEBUG_2             meanbelow    "H2:LSC-LA_PTRR_NORM"     threshold=100000.
#H2:DEBUG_3             valueabove   "H2:LSC-LA_PTRR_NORM"     threshold=0.0
#H2:DEBUG_4             valuebelow   "H2:LSC-LA_PTRR_NORM"     threshold=100000.

###################
#                  #
#  End H2 Section   #
#                    #
#######################


#######################
#                    #
#  Begin L1 Section #
#                  #
###################

L1:MC_F_peaking        abspowerabove "L1:IOO-MC_F"  freqlo=3000 freqhi=7000 threshold=19.
L1:MC_problem          boolean       "L1:Both_arms_locked & L1:MC_F_peaking"

# Violin mode fundamental
L1:DARM_CTRL_violin_0  abspowerabove "L1:LSC-DARM_CTRL" freqlo=340 freqhi=345 threshold=6.55
L1:DARM_problem_0      boolean       "L1:Both_arms_locked & L1:DARM_CTRL_violin_0"

# Close to the violin mode fundamental
L1:DARM_CTRL_violin_0_plus  abspowerabove "L1:LSC-DARM_CTRL" freqlo=345 freqhi=355 threshold=3.2
L1:DARM_problem_0_plus      boolean       "L1:Both_arms_locked & L1:DARM_CTRL_violin_0_plus"

# Violin mode 2nd harmonic
L1:DARM_CTRL_violin_1  abspowerabove "L1:LSC-DARM_CTRL" freqlo=680 freqhi=690 threshold=71.
L1:DARM_problem_1      boolean       "L1:Both_arms_locked & L1:DARM_CTRL_violin_1"

# 400-450 Hz
L1:DARM_CTRL_400_450   abspowerabove "L1:LSC-DARM_CTRL" freqlo=400 freqhi=450 threshold=1000.
L1:DARM_problem_2      boolean       "L1:Both_arms_locked & L1:DARM_CTRL_400_450"

###################
#                  #
#  End L1 Section   #
#                    #
#######################
