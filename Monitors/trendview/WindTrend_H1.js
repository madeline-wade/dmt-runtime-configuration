{
    "server-name" : "WindTrend_H1",
    "trend-name"  : "WindTrend_H1",
    "channels" : [
        {
            "channel-name"  : "H1:PEM-CS_WIND_ROOF_WEATHER_MPH",
            "view-name"     : "H1:DMT-WIND_CS_ROOF_SPEED",
            "trend-name"    : "H1:DMT-WIND_CS_ROOF_SPEED",
	    "viewer-epoch"  : 172800,
	    "viewer-stride" : 15
        },
        {
            "channel-name"  : "H1:PEM-EX_WIND_ROOF_WEATHER_MPH",
            "view-name"     : "H1:DMT-WIND_EX_ROOF_SPEED",
            "trend-name"    : "H1:DMT-WIND_EX_ROOF_SPEED",
	    "viewer-epoch"  : 172800,
	    "viewer-stride" : 15
        },
        {
            "channel-name"  : "H1:PEM-EY_WIND_ROOF_WEATHER_MPH",
            "view-name"     : "H1:DMT-WIND_EY_ROOF_SPEED",
            "trend-name"    : "H1:DMT-WIND_EY_ROOF_SPEED",
	    "viewer-epoch"  : 172800,
	    "viewer-stride" : 15
        },
        {
            "channel-name"  : "H1:PEM-MX_WIND_ROOF_WEATHER_MPH",
            "view-name"     : "H1:DMT-WIND_MX_ROOF_SPEED",
            "trend-name"    : "H1:DMT-WIND_MX_ROOF_SPEED",
	    "viewer-epoch"  : 172800,
	    "viewer-stride" : 15
        },
        {
            "channel-name"  : "H1:PEM-MY_WIND_ROOF_WEATHER_MPH",
            "view-name"     : "H1:DMT-WIND_MY_ROOF_SPEED",
            "trend-name"    : "H1:DMT-WIND_MY_ROOF_SPEED",
	    "viewer-epoch"  : 172800,
	    "viewer-stride" : 15
        }
    ]
}
