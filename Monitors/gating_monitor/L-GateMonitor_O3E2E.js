{
    "ifo" : "L1",
    "stride" : 1.0,
    "osc-file" : "l1_locked.osc",
    "osc-cond" : "L1:IFO_READY",
    "monitor-prefix" : "GATM",
    "trend-name" : "L1_GATEM",
    // "trend-file-path" : "$DMTRENDOUT/L-L1_GATEM_M-%g-3600.gwf",
    "gates"  : [
	{"name"        : "OMC ADC Overflow",
	 "bit-number"  : 1,
	 "bit-channel" : "L1:DMT-DQ_VECTOR_GATED"
	},
 	{"name"        : "ETMY ESD DAC Overflow",
	 "bit-number"  : 2,
	 "bit-channel" : "L1:DMT-DQ_VECTOR_GATED"
	},
 	{"name"        : "ETMX ESD DAC Overflow",
	 "bit-number"  : 4,
	 "bit-channel" : "L1:DMT-DQ_VECTOR_GATED"
	},
 	{"name"        : "Super-loud band self-gate",
	 "bit-number"  : 5,
	 "bit-channel" : "L1:DMT-DQ_VECTOR_GATED",
	 "gate-threshold" : 3e-7,
	 "thresh-channel" : "L1:DMT-BLRMS_STRAIN_70_110",
	 "plot" : {
	     "xmin" : 1e-8,
	     "xmax" : 2e-5,
	     "logbin" : true,
	     "xlog" : true,
	     "ylog" : true
	 }
	},
 	{"name"        : "Scattering band self-gate",
	 "bit-number"  : 6,
	 "bit-channel" : "L1:DMT-DQ_VECTOR_GATED",
	 "gate-threshold" : 2e-7,
	 "thresh-channel" : "L1:DMT-BLRMS_STRAIN_25_50",
	 "plot" : {
	     "xmin" : 1e-8,
	     "xmax" : 3e-5,
	     "logbin" : true,
	     "xlog" : true,
	     "ylog" : true
	 }
	},
 	{"name"        : "All Gates",
	 "bit-number"  : 3,
	 "bit-channel" : "L1:DMT-DQ_VECTOR_GATED"
	}
    ],
    "segments" : [
        {"name" : "L1:DMT_ALL_GATES_ACTIVE",
         "gate" : "All Gates",
         "mode" : "active"
        },
        {"name" : "L1:DMT_ALL_GATES_GT_5SEC",
         "gate" : "All Gates",
         "mode" : "long",
         "max-time" : 5.0
        },
        {"name" : "L1:DMT_ALL_GATES_GT_2PCT_1800",
         "gate" : "All Gates",
         "mode" : "fract_lock",
         "fraction" : 0.02,
         "average-time" : 1800.0
        },
        {"name" : "L1:DMT_ALL_GATES_GT_2PCT_7200",
         "gate" : "All Gates",
         "mode" : "fract_lock",
         "fraction" : 0.02,
         "average-time" : 7200.0
        }
    ],
    "trends" : [
	{"name" : "ALL_GATES",
	 "gate" : "All Gates",
	 "enable-dead" : true,
	 "enable-discr" : false
	},
	{"name" : "SLOUD_GATE",
	 "gate" : "Super-loud band self-gate",
         "enable-dead"  : false,
         "enable-discr" : true
        },
        {"name" : "SCATTR_GATE",
         "gate" : "Scattering band self-gate",
         "enable-dead"  : false,
         "enable-discr" : true
        }
    ]
}
