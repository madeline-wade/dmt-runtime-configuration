#! /usr/bin/env python

# generates a waveform for SenseMonitor

import h5py
import numpy
from pycbc import DYN_RANGE_FAC
from pycbc.filter import get_cutoff_indices, sigma
from pycbc.psd import interpolate
from pycbc.types import load_frequencyseries, FrequencySeries
from pycbc.types.array import zeros, Array
from pycbc.waveform import get_waveform_filter

# waveform generation options
mass1 = 30.0
mass2 = 30.0
distance = 1.0 / DYN_RANGE_FAC
approximant = "SEOBNRv2_ROM_DoubleSpin"
canonical_snr = 8.0
fhigh = None
flow = 20

# output file options
output_file = "SenseMonitor_BBH_30_30.txt"

# PSD options
psd_file = "H1_O1_PSD.txt"

# get PSD that use be used to test inspiral range calculation of filter
# and its length will set the length of the output file
psd = load_frequencyseries(psd_file)
psd = psd.astype(numpy.complex128)

# get FrequencySeries of filter
delta_t = 1.0 / ((len(psd) - 1) * 2 * psd.delta_f)
out = zeros(len(psd), dtype=numpy.complex64)
htilde = get_waveform_filter(out,
                         mass1=mass1, mass2=mass2,
                         approximant=approximant,
                         f_lower=flow, delta_f=psd.delta_f,
                         delta_t=delta_t,
                         distance = distance)
htilde = htilde.astype(numpy.complex128) * (1e9 * 4000)

# calculate the inspiral range with example PSD file
sigma_value = sigma(htilde, psd=psd, low_frequency_cutoff=flow)
horizon_distance = sigma_value / canonical_snr
inspiral_range = horizon_distance / 2.26
print "Inspiral range from pycbc with example PSD is:", inspiral_range

# get product of filter with itself
N = (len(htilde)-1) * 2
kmin, kmax = get_cutoff_indices(flow, fhigh, htilde.delta_f, N)
ht = htilde[kmin:kmax]
sq = ht.data.conj() * ht.data

# conver to float64
# sq is already a real number but has complex128 data type
sq = sq.real

# make a FrequencySeries that has the length of the PSD
# and contains the square of the filter
out = zeros(len(psd), dtype=sq.dtype)
out[kmin:kmax] = Array(sq)
sq_fs = FrequencySeries(out, delta_f=htilde.delta_f)
sq_fs = interpolate(sq_fs, 0.25)

# constants
SOLARMASS = 1.989e30
NEWTONS_G = 6.67e-11
C = 299792458
METER_PER_MEGAPARSEC = 3.086E22
RHO_o = 8
PI = 3.141592654
ARM_LENGTH_NM = 4e12

# write unwhitened integrand to file
arr = 4.0 * sq_fs.astype(numpy.float64) / (8 * 2.26)**2 / DYN_RANGE_FAC**2
arr.save(output_file)

# exit
print "Finish"
