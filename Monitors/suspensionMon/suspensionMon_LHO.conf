#$Id$
#====================================
# file path to the text output data file
Data_File_Prefix : suspensionMon_data

# file path to the log file
Log_File_Prefix : suspensionMon_log

# length of data to process in seconds (0 for infinity)
Total_Runtime    : 0

# of bins which composes each histogram
Number_Bins      : 20

# Overall allows/disallows triggers to be sent
Trigger_ON/OFF   : 1

# Interval for which to process data (in seconds)
Loop_Time        : 60 

# Trend One Prefix
Trend_One_Prefix : susTrend

## The first two characters of the trend prefixes are used for the interferometer
## The class adds GPS time and a suffix to the trendfile name

## File modified 11-18-2003 to adjust long-term thresholds to 10 cts on large optics, and 5 cts.
## on small optics above in-lock average. Also added was a 20Hz highpass filter.

##### Channel Configuration -- below are the formats for the allowable entries
#####   some specific items are explained immediately following.  All Filter, Glitch,
#####   Statistic, and Result entries are specific to the preceeding Channel entry. 
#####   (Note: '|' implies either/or, '[]' imply optional arguments, and unbracketed
#####    items are literal strings.)
# Channel <channel_name>  (<osc_name> | 0)  [<filter_poles>  <filter_zeroes> [<gain>] <filter_name>]
#  Filter <filter_name> <filter_poles> <filter_zeroes> [<gain>] [<use_filter_name>]
#  Glitch <glitch_name> <threshold_level> <time_constant>
#  Statistic <statistic_name> <statistic_type> <short_term> <long_term> [<initial_value>]
#  Result <result_name> <result_type> <cumulative?> [<update_interval>] <output_mask> [<output_file_prefix> ...]
##### Explanation of items: 
#####   <filter_poles> and <filter_zeroes> are quoted pairs of real numbers which are combined to
#####      form complex numbers, e.g. "1.0 2.0 3.0 4.0" -> the set of poles/zeroes {1+2i , 3+4i}
#####   <use_filter_name> should be the name of a previous filter for that channel. If none is
#####      supplied, raw channel data is used.
#####   <time_constant> is the interval (in seconds) for which a glitch can be put on "pause",
#####      in case the glitch-satisfying conditions in the (possibly filtered) data return.
#####   <statistic_type> currently can be one of "Mean" "RMS" "Kurt" or "susStatistic"
#####   <short_term> number of intervals to use for short-term rms computation
#####   <long_term> number of intervals to use for long-term rms computation
#####   <result_type> currently can be one of "Log" or "Events"
#####   <cumulative?> is '1' or '0', and, if true, implies that the statistic/result will not be
#####      be reset at the start of each log interval.  Note that <update_interval> will default
#####      to 3600sec if not included while the statistic/result is refreshable.
#####   <initial_value> provides an initial value to be used for the statistic.  Defaults to '0'.
#####      Note if <cumulative?> is '0', you must define both <update_interval> and <initial_value>.
#####   <output_mask> output bitset -- turn bit on for output re-direction:
#####      2^0 -- standard output
#####      2^1 -- output to data file (defaults to Data_File_Path if <output_file_prefix> is not set
#####      2^2 -- output to log file (if both data and log file bits are set, second
#####             <output_file_prefix> refers to log file prefix
#####      2^3 -- output to Trigger Manager
#####      2^4 -- output to DMTViewer
#####      2^5 -- output to Trend file
#####   <output_file_prefix> when not an absolute path, outputs to ${DMTxxxxx}/<output_file_prefix>*.
#####      Otherwise output goes to <output_file_prefix>*.



##--------*********H2CHANNELS**********-------------


## ------------------------------------------------------------------
Channel  H2:SUS-ETMY_SENSOR_SIDE H2:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H2:DMT-SUSP_ETMY_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500 

## ------------------------------------------------------------------
Channel  H2:SUS-ETMX_SENSOR_SIDE H2:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H2:DMT-SUSP_ETMX_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H2:SUS-ITMX_SENSOR_SIDE H2:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H2:DMT-SUSP_ITMX_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H2:SUS-ITMY_SENSOR_SIDE H2:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H2:DMT-SUSP_ITMY_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H2:SUS-RM_SENSOR_SIDE H2:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H2:DMT-SUSP_RM_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H2:SUS-BS_SENSOR_SIDE H2:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H2:DMT-SUSP_BS_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H2:SUS-MMT1_SENSOR_SIDE H2:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H2:DMT-SUSP_MMT1_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H2:SUS-MMT2_SENSOR_SIDE H2:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H2:DMT-SUSP_MMT2_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H2:SUS-MMT3_SENSOR_SIDE H2:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H2:DMT-SUSP_MMT3_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H2:SUS-MC1_SENSOR_SIDE H2:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H2:DMT-SUSP_MC1_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H2:SUS-MC2_SENSOR_SIDE H2:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H2:DMT-SUSP_MC2_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H2:SUS-MC3_SENSOR_SIDE H2:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H2:DMT-SUSP_MC3_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H2:SUS-SM1_SENSOR_SIDE H2:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H2:DMT-SUSP_SM1_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H2:SUS-SM2_SENSOR_SIDE H2:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H2:DMT-SUSP_SM2_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H2:SUS-FMX_SENSOR_SIDE H2:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H2:DMT-SUSP_FMX_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H2:SUS-FMY_SENSOR_SIDE H2:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H2:DMT-SUSP_FMY_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500


##--------*********H1CHANNELS**********------------- 


## ------------------------------------------------------------------
Channel  H1:SUS-ETMX_SENSOR_SIDE H1:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H1:DMT-SUSP_ETMX_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H1:SUS-ETMY_SENSOR_SIDE H1:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H1:DMT-SUSP_ETMY_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H1:SUS-ITMX_SENSOR_SIDE H1:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H1:DMT-SUSP_ITMX_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H1:SUS-ITMY_SENSOR_SIDE H1:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H1:DMT-SUSP_ITMY_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H1:SUS-RM_SENSOR_SIDE H1:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H1:DMT-SUSP_RM_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H1:SUS-BS_SENSOR_SIDE H1:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H1:DMT-SUSP_BS_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H1:SUS-MMT1_SENSOR_SIDE H1:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H1:DMT-SUSP_MMT1_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H1:SUS-MMT2_SENSOR_SIDE H1:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H1:DMT-SUSP_MMT2_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H1:SUS-MMT3_SENSOR_SIDE H1:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H1:DMT-SUSP_MMT3_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H1:SUS-MC1_SENSOR_SIDE H1:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H1:DMT-SUSP_MC1_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H1:SUS-MC2_SENSOR_SIDE H1:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H1:DMT-SUSP_MC2_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H1:SUS-MC3_SENSOR_SIDE H1:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H1:DMT-SUSP_MC3_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500

## ------------------------------------------------------------------
Channel  H1:SUS-SM_SENSOR_SIDE H1:SV_Operator_Go  "-4.1978 20.9740 -4.1978 -20.9740 -15.4214 0 -0.2594 0 -0.0367 0.1834 -0.0367 -0.1834""0 31.4138 0 -31.4138 0 0.1273 0 -0.1273 0 0" 7.2118 bpf

 Statistic H1:DMT-SUSP_SM_SIDE  susStatistic 1 10 

 Result  runOutput-log	susLog   0 600 32 

 Result  trigger-log	susEvent 0 600 8
 4.4 500


