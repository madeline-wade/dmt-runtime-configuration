* * * * * /usr/bin/SPI.pl --gds-config=$HOME/dmtspi --cache=/gds/dmt/dmtspi/cache
*/10 * * * * /usr/bin/USAGE.tcsh --gds-config=$HOME/dmtspi
* * * * * $HOME/status/status-monitor-l1dmt1
*/5 * * * * $HOME/status/all_files-llo
3 * * * * $HOME/scripts/dpush-summary
10 * * * * (/bin/rsync --verbose -W --archive --compress --timeout=3600 --exclude conlog/  --exclude current --exclude frames/ /gds/dmt/trends/ ldasadm@ldas-cit.ligo.caltech.edu:/archive/frames/dmt/L1/trends > /var/tmp/rsync.trends.out 2>&1)
10 2 * * * $HOME/scripts/sensemon_rename
