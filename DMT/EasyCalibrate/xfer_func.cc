//
// xfer_func creates a transfer function.
//
#include "FilterDesign.hh"
#include "FSeries.hh"
#include <iostream>
#include <fstream>

using namespace std;

void
print_FSeries(const FSeries& fs, std::ostream& of) {
  size_t N = fs.getNStep();
  for (size_t i=0; i<N; i++) {
    double f = fs.getBinF(i);
    of << f << "  " << fs(f).Mag() << "  " << fs(f).Arg() << endl;
  }
}

int
main(int argc, const char* argv[]) {
    double rate(16384.0);
    double fMin(0.0), fMax(4096), dF(0.25);

    //----------------------------------  Design the filter.
    FilterDesign fd(argv[1], rate);

    //----------------------------------  Get the transfer function.
    FSeries xf;
    fd.Xfer(xf, fMin, fMax, dF);

    //----------------------------------  Write it out
    ofstream of("transfer.txt");
    print_FSeries(xf, of);
}
