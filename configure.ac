# Process this file with autoconf to produce a configure script
# Author: Edward Maros

AC_INIT([gds-dmt-runtime-configuration],[3.0.0],[],[],[https://wiki.ligo.org/Computing/DASWG/DMT])
AC_SUBST([PACKAGE_SOURCE_URL],
  [https://software.igwn.org/lscsoft/source/])

configure_flags="$*"
AC_CONFIG_AUX_DIR([config])
AC_CONFIG_MACRO_DIR([config])
AM_INIT_AUTOMAKE([foreign subdir-objects])
m4_ifdef([AM_SILENT_RULES],[AM_SILENT_RULES([no])])
AC_SUBST([configure_flags])

# Checks for package options
AC_ARG_ENABLE([lho],[AS_HELP_STRING([--enable-lho],
[enable lho (default is no)])],,[enable_lho=no])
AM_CONDITIONAL([LHO], [test x${enable_lho} = xyes])

# Checks for package options
AC_ARG_ENABLE([llo],[AS_HELP_STRING([--enable-llo],
[enable llo (default is no)])],,[enable_llo=no])
AM_CONDITIONAL([LLO], [test x${enable_llo} = xyes])

AS_IF([test "x${enable_lho}" = xno -a "x${enable_llo}" = xno],
      [AC_MSG_WARN([Normally used with either --enable-lho or --enable-llo])] )

# Checks for package options
AC_ARG_ENABLE([monitor],[AS_HELP_STRING([--enable-monitor],
[enable monitor (default is no)])],,[enable_monitor=no])
AM_CONDITIONAL([MONITOR], [test x${enable_monitor} = xyes])

# Checks for package options
AC_ARG_ENABLE([redundant],[AS_HELP_STRING([--enable-redundant],
[enable redundant (default is no)])],,[enable_redundant=no])
AM_CONDITIONAL([REDUNDANT], [test x${enable_redundant} = xyes])

# Checks for package options
AC_ARG_ENABLE([replay],[AS_HELP_STRING([--enable-replay],
[enable replay (default is no)])],,[enable_replay=no])
AM_CONDITIONAL([REPLAY], [test x${enable_replay} = xyes])

# Checks for package options
AC_ARG_ENABLE([test],[AS_HELP_STRING([--enable-test],
[enable test (default is no)])],,[enable_test=no])
AM_CONDITIONAL([TEST], [test x${enable_test} = xyes])

AS_IF([test "x${enable_monitor}" = xno -a "x${enable_redundant}" = xno -a \
            "x${enable_replay}" = xno -a "x${enable_testt}" = xno],
      [AC_MSG_WARN([Normally used with --enable-monitor, --enable-redundant,
                    --enable-replay or --enable-test])] )

# Checks for host
AC_CANONICAL_HOST

# Checks for basic system programs.
AC_PATH_PROG(BASH, bash)
AC_PATH_PROG(CSH,  csh)
AC_PATH_PROG(PERL, perl)
AC_PATH_PROG(PROCSETUP,procsetup)
AC_PATH_PROG(PROCLISTPARS,procmgt_list_param_files)

AC_CONFIG_FILES([
  Makefile
  aLigo/Makefile
  aLigo/scripts/Makefile
  DMT/Makefile
  DMT/DMTGen/Makefile
  DMT/EasyCalibrate/Makefile
  Monitors/Makefile
  Monitors/BicoMon/Makefile
  Monitors/BitTest/Makefile
  Monitors/Omega_c/Makefile
  Monitors/PlaneMon/Makefile
  Monitors/SegGener/Makefile
  Monitors/SenseMonitor/Makefile
  Monitors/SenseMonitor/data/Makefile
  Monitors/blrms/Makefile
  Monitors/dq-module/Makefile
  Monitors/dvTest/Makefile
  Monitors/endtimes/Makefile
  Monitors/gating_monitor/Makefile
  Monitors/trendview/Makefile
  replay/Makefile
  Services/Makefile
  Services/TrigMgr/Makefile
  scripts/Makefile
  Spi/Makefile
  status/Makefile
])

AC_OUTPUT
