# dmt-runtime-config-NEXT:
  - Fix dmt process configuration errors
  - Remove unused Monitor, Redundant process lists leaving e.g. MonitorDQ
  - use ~john.zweizig/public_html/ for LHO O3replay output
  - add admin documentation to $HOME/scripts/README
  - New next-flood and purgedir scripts
  - o3replay cron table files

